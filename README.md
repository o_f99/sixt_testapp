# README #

### What is this repository for? ###

* HTTP/JSON App Built for Sixt Interview Process. Sorry for the Horrible Design, my design skills are bad.

### How do I get set up? ###

* Built on the latest XCode with Deployment target 10.1

* This Projects uses CocoaPods for Dependency Management. The Pods required for this project are
* Alamofire instead of NSURLSession for better validation,async behaviour and callbacks
* Gloss for more concise Parsing of JSON Response,
* AlamofireImage for Easy Async Image Download

In case of any difficulties regarding setting up or any other questions please contact me.