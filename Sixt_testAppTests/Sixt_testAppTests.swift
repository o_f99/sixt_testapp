//
//  Sixt_testAppTests.swift
//  Sixt_testAppTests
//
//  Created by omer on 12/6/16.
//  Copyright © 2016 omer. All rights reserved.
//

import XCTest
@testable import Sixt_testApp

class Sixt_testAppTests: XCTestCase {
    var mainVC : ViewController!
    
    /* Sample UITabelView Testing */
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        mainVC = storyboard.instantiateViewController(withIdentifier: "mainVC") as! ViewController
        let _ = mainVC.view
        mainVC.tableView.reloadData()
    }
    
    func testNumberofRowsinTableView() {
        
        let numberOfCars = mainVC.viewModel.numberOfCars()
        XCTAssert(mainVC.tableView.numberOfRows(inSection: 0)==numberOfCars)
    }
}
