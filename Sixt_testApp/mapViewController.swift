//
//  mapViewController.swift
//  Sixt_testApp
//
//  Created by omer on 12/10/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Alamofire
import AlamofireImage

class MapViewController : UIViewController,MKMapViewDelegate {
    
    // MARK: - Variables
    var regionRadius: CLLocationDistance = 1000
    var mapViewModel : mapViewModel!

    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var licensePlate: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    // MARK: - IBActions
    @IBAction func dismissViewController(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {})
        
    }
    
    // MARK: - Functions
    
    override func viewDidLoad() {
        /*
            Setting Up Delegate,Annotations and UI
        */
        mapView.delegate = self
        mapView.addAnnotation(mapViewModel.getObject())
        
        self.centerMapOnLocation(location: mapViewModel.getCoordinates())
        
        self.modelName.text = mapViewModel.getModelName()
        self.name.text = mapViewModel.getName()
        self.make.text = mapViewModel.getMake()
        self.color.text = mapViewModel.getColor()
        self.licensePlate.text = mapViewModel.getLicensePlate()
        self.imageView.image = mapViewModel.getImage()
    }
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // MARK: - MapView Delegates

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let annotation = annotation as? Car {
            let id = "pin"
            var view: MKPinAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: id)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: id)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
            }
            return view
        }
        return nil
    }
    
}
