//
//  Constants.swift
//  Sixt_testApp
//
//  Created by omer on 12/15/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation

struct Constants {
    
    /*
        Global Constants
    */
    
    static let requestURL = "http://www.codetalk.de/cars.json"
    static let imgNotAvailable = "imageNotAvailable"
    static let webRequestRetryAttempts = 3
}
