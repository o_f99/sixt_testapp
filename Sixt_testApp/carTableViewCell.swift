//
//  carTableViewCell.swift
//  Sixt_testApp
//
//  Created by omer on 12/14/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation
import UIKit

class carTableViewCell : UITableViewCell {
    
    /*
        Custom Table View Cell
    */
    
    @IBOutlet weak var tableViewCellImage: UIImageView!
    
}
