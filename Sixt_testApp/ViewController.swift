//
//  ViewController.swift
//  Sixt_testApp
//
//  Created by omer on 12/6/16.
//  Copyright © 2016 omer. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import MapKit

/* Built using MVVM approach instead of MVC */

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - variables
    var viewModel : ViewModel!
    private var observerContext = 0
    
    // MARK: - Functions
    override func viewDidLoad() {
        
        /*
            Setting up Delegate,DataSource,ViewModel,KVO
        */
        
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //Adding viewModel 
        self.viewModel = ViewModel()

        activityView.startAnimating()
        
        //Adding Observer
        self.viewModel!.addObserver(self, forKeyPath: "car", options: NSKeyValueObservingOptions.new, context: &observerContext)
        self.viewModel!.addObserver(self, forKeyPath: "failed", options: NSKeyValueObservingOptions.new, context: &observerContext)

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &observerContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if let key = keyPath {
            if(key=="car"){
                self.activityView.stopAnimating()
                self.overlayView.removeFromSuperview()
                self.tableView.reloadData()
                
            }else{
                alertViewDisplay()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue1" , let nextScene = segue.destination as? MapViewController ,
            let indexPath = self.tableView.indexPathForSelectedRow {
            
            nextScene.mapViewModel = mapViewModel(car: viewModel.getCar(index: indexPath.row))
        }
    }
    
    
    func alertViewDisplay(){
        let alertController = UIAlertController(title: "Failed to Start", message: "No Network Connectivity or Server is not responding. Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}



// MARK: - Table View Data Source and Delegate Methods

extension ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCars()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as? carTableViewCell {
            cell.tableViewCellImage.image = viewModel.getImage(index: indexPath.row)
            return cell as UITableViewCell
        }else{
            return (tableView.dequeueReusableCell(withIdentifier: "cell1"))!
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
}







