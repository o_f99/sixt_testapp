//
//  mapViewModel.swift
//  Sixt_testApp
//
//  Created by omer on 12/15/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation
import MapKit

class mapViewModel {
    
    var detailObject : Car
    
    init(car:Car) {
        detailObject = car
    }
    
    func getCoordinates() -> CLLocationCoordinate2D {
        return detailObject.coordinate
    }
    
    func getObject() -> Car {
        return detailObject
    }
    
    func getModelName() -> String {
        return detailObject.modelName!
    }
    
    func getName() -> String {
        return detailObject.name!
    }
    
    func getMake() -> String {
        return detailObject.make!
    }
    
    func getColor() -> String {
        return detailObject.color!
    }
    
    func getLicensePlate() -> String {
        return detailObject.licensePlate!
    }
    
    func getImage() -> UIImage {
        return detailObject.image!
    }
    
}
