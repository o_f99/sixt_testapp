//
//  CarsViewModel.swift
//  Sixt_testApp
//
//  Created by omer on 12/14/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

class ViewModel : NSObject {
    
    lazy dynamic var car = [Car]()
    var retryAttempts: Int
    dynamic var failed : Bool
    var timer : Timer?
    
    // MARK: - Public Methods
    
    override init(){
        retryAttempts = Constants.webRequestRetryAttempts
        failed = false
        super.init()
        sendRequest()
    }
    
    func numberOfCars() -> Int{
        return self.car.count
    }
    
    func getId(index: Int) -> String {
        return car[index].id!
    }
    
    func getImage(index: Int) -> UIImage {
        return car[index].image
    }
    
    func getCar(index: Int) -> Car {
        return car[index]
    }
    
    func getLat(index: Int) -> Double {
        return car[index].latitude!
    }
    
    func getLong(index: Int) -> Double {
        return car[index].longitude!

    }
    
    func correctImageURL( modelIdentifier:String, color:String)->String{
        return "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/\(modelIdentifier)/\(color)/2x/car.png"
    }
    
}


// MARK: - JSON Request,Parsing and Handling Methods

extension ViewModel {
    
    
    func sendRequest()
    {
        // Used Alamofire instead of NSURLSession for better validation,async behaviour and callbacks
        Alamofire.request(Constants.requestURL).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                if let time = self.timer {
                    time.invalidate()
                }
                if let JSON = response.result.value {
                    
                    // Used Gloss to reduce verbous JSON parsing code
                    guard let val = JSON as? [JSON] else {
                        return;
                    }
                    guard let cars = [Car].from(jsonArray:val) else {
                        return;
                    }
                    self.wrapper(retryAttempts: self.retryAttempts,tempCar: cars, wrapperCode:self.request)
                }
            case .failure:
                self.failed = true
                self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.sendRequest), userInfo: nil, repeats: true);
                
            }
        }
    }
    
    
    func request(tempCar:[Car],success:@escaping (Car,DataResponse<Data>) -> Void,failure:@escaping (Car,DataResponse<Data>) -> Void)->Void{
        for i in 0..<tempCar.count {
            if tempCar[i].hasPlaceHolderImg, tempCar[i].iterationRequired {
                tempCar[i].imageURL = self.correctImageURL(modelIdentifier: tempCar[i].modelIdentifier!, color: tempCar[i].color!)
                let url = URL(string: "\(tempCar[i].imageURL!)")!
                Alamofire.request(url).validate().responseData { response in
                    
                    switch response.result {
                    case .success:
                        success(tempCar[i],response)
                    case .failure:
                        failure(tempCar[i],response)
                    }
                    
                    //We Dont wait for all the images to download
                    if i==10{
                        self.car = tempCar
                    }
                }
            }
        }
    }
    
    
    func wrapper(retryAttempts:Int,tempCar:[Car],
                 wrapperCode:@escaping ([Car],@escaping(Car,DataResponse<Data>)->Void,@escaping(Car,DataResponse<Data>)->Void)->Void){
        var localParam : Int = retryAttempts
        wrapperCode(tempCar,{ currentCar,response in
            currentCar.image = UIImage(data: response.data!, scale:1)
            currentCar.hasPlaceHolderImg = false
            currentCar.iterationRequired = false
        },{ currentCar,response in
            //In Case of failure HTTP request sent again
            if(localParam>0){
                localParam -= 1
                self.wrapper(retryAttempts: localParam, tempCar: [currentCar], wrapperCode:self.request)
            }
        })
        
    }
    
}








