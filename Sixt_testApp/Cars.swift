//
//  Cars.swift
//  Sixt_testApp
//
//  Created by omer on 12/6/16.
//  Copyright © 2016 omer. All rights reserved.
//

import Foundation
import Gloss
import MapKit

class Car: NSObject, Decodable,MKAnnotation {
    
    var id : String?
    var modelIdentifier: String?
    var modelName: String?
    var name: String?
    var make: String?
    var carImageUrl: String?
    var latitude: Double?
    var longitude: Double?
    var imageURL: String?
    var color : String?
    var licensePlate: String?
    var image : UIImage!
    var hasPlaceHolderImg : Bool
    var iterationRequired: Bool
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    
    required init?(json: JSON) {
        self.id = "id" <~~ json
        self.modelIdentifier = "modelIdentifier" <~~ json
        self.modelName = "modelName" <~~ json
        self.name = "name" <~~ json
        self.make = "make" <~~ json
        self.carImageUrl = "carImageUrl" <~~ json
        self.latitude = "latitude" <~~ json
        self.longitude = "longitude" <~~ json
        self.title = "Title"
        self.subtitle = "SubTitle"
        self.coordinate = CLLocationCoordinate2DMake(latitude!, longitude!)
        self.imageURL = "carImageUrl" <~~ json
        self.color = "color" <~~ json
        self.licensePlate = "licensePlate" <~~ json
        self.image = UIImage(named: Constants.imgNotAvailable)
        self.hasPlaceHolderImg = true
        self.iterationRequired = true
        self.title = self.name
        self.subtitle = self.modelName
    }
    

}
